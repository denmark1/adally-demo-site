<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'adally_demo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}XE7BE}}XJmcPKIj8Cm=Uh W<6CDfKoX#[t2|eGW}ZVybyQRoVzuV<?q 6{YAN5F' );
define( 'SECURE_AUTH_KEY',  '@Z4)D%/67RRDHBW@r+b1$EwZ,0Ai+0WKE06-A,`J/JWJF1T][%ixnVc<}?;AKyH0' );
define( 'LOGGED_IN_KEY',    ':(= Q}2THoCYMV%8>=vzBxMFdz,kq-&GvB85GyOJ{+Oq>>!;(A$R}>oE:A(t[&~e' );
define( 'NONCE_KEY',        'TDlOzF9![a&-PLzPX)H:qD<%17Wg]I,yn/exlbhW*h6!>4ylPdqgKd#(gXE|c*fy' );
define( 'AUTH_SALT',        '+z*eO_TsTRWO({r3q-b/-%Aa_eOV>JLZhNy8KoaW9J;5nV)`l$&&jeR m$o3G?pO' );
define( 'SECURE_AUTH_SALT', 's3@gA]]oU%]8vOOH`5Z1=$)EjK2Lg|[oEJ E|jE([J!1`cXL(xA_XSfGAw?WI?!|' );
define( 'LOGGED_IN_SALT',   'R)OVsU/}uX8U+|L]PEumU*RDSU@kN#(H4<E&LtY.kJXC^h8v ;i=[6Hme5OOb$q4' );
define( 'NONCE_SALT',       'r~)N1;BPW66|<4ld|+IH3Dbj>q :ebeT&S!jE])m7LaI);B%Yjdl}cQI_ceVOT7%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ad_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
